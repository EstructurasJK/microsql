﻿namespace MicroSQL
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.menu_MainTools = new System.Windows.Forms.Panel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.menu_1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.menu_traduction = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.MainSplit = new System.Windows.Forms.SplitContainer();
            this.panel_DBList = new System.Windows.Forms.FlowLayoutPanel();
            this.SubSplit = new System.Windows.Forms.SplitContainer();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.txt_Editor = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.menu_SQLTools = new System.Windows.Forms.FlowLayoutPanel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.panel_Salida = new System.Windows.Forms.Panel();
            this.menu_MainTools.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.menu_1.SuspendLayout();
            this.menu_traduction.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainSplit)).BeginInit();
            this.MainSplit.Panel1.SuspendLayout();
            this.MainSplit.Panel2.SuspendLayout();
            this.MainSplit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SubSplit)).BeginInit();
            this.SubSplit.Panel1.SuspendLayout();
            this.SubSplit.Panel2.SuspendLayout();
            this.SubSplit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.txt_Editor.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.menu_SQLTools.SuspendLayout();
            this.panel10.SuspendLayout();
            this.SuspendLayout();
            // 
            // menu_MainTools
            // 
            this.menu_MainTools.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(91)))), ((int)(((byte)(127)))));
            this.menu_MainTools.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.menu_MainTools.Controls.Add(this.flowLayoutPanel1);
            this.menu_MainTools.Controls.Add(this.panel3);
            resources.ApplyResources(this.menu_MainTools, "menu_MainTools");
            this.menu_MainTools.Name = "menu_MainTools";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(38)))), ((int)(((byte)(69)))));
            this.flowLayoutPanel1.Controls.Add(this.menu_1);
            this.flowLayoutPanel1.Controls.Add(this.menu_traduction);
            this.flowLayoutPanel1.Controls.Add(this.panel5);
            this.flowLayoutPanel1.Controls.Add(this.panel6);
            resources.ApplyResources(this.flowLayoutPanel1, "flowLayoutPanel1");
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            // 
            // menu_1
            // 
            this.menu_1.Controls.Add(this.label3);
            this.menu_1.Controls.Add(this.label2);
            this.menu_1.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.menu_1, "menu_1");
            this.menu_1.Name = "menu_1";
            this.menu_1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.menu_1_MouseClick);
            this.menu_1.MouseEnter += new System.EventHandler(this.menu_1_MouseEnter);
            this.menu_1.MouseLeave += new System.EventHandler(this.menu_1_MouseLeave);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.GreenYellow;
            this.label3.Name = "label3";
            this.label3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.menu_1_MouseClick);
            this.label3.MouseEnter += new System.EventHandler(this.menu_1_MouseEnter);
            this.label3.MouseLeave += new System.EventHandler(this.menu_1_MouseLeave);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.SystemColors.MenuBar;
            this.label2.Name = "label2";
            this.label2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.menu_1_MouseClick);
            this.label2.MouseEnter += new System.EventHandler(this.menu_1_MouseEnter);
            this.label2.MouseLeave += new System.EventHandler(this.menu_1_MouseLeave);
            // 
            // menu_traduction
            // 
            this.menu_traduction.Controls.Add(this.label11);
            this.menu_traduction.Controls.Add(this.label12);
            this.menu_traduction.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.menu_traduction, "menu_traduction");
            this.menu_traduction.Name = "menu_traduction";
            this.menu_traduction.Paint += new System.Windows.Forms.PaintEventHandler(this.panel4_Paint);
            this.menu_traduction.MouseClick += new System.Windows.Forms.MouseEventHandler(this.menu_traduction_MouseClick);
            this.menu_traduction.MouseEnter += new System.EventHandler(this.menu_traduction_MouseEnter);
            this.menu_traduction.MouseLeave += new System.EventHandler(this.menu_traduction_MouseLeave);
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.ForeColor = System.Drawing.Color.Turquoise;
            this.label11.Name = "label11";
            this.label11.MouseClick += new System.Windows.Forms.MouseEventHandler(this.menu_traduction_MouseClick);
            this.label11.MouseEnter += new System.EventHandler(this.menu_traduction_MouseEnter);
            this.label11.MouseLeave += new System.EventHandler(this.menu_traduction_MouseLeave);
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.ForeColor = System.Drawing.SystemColors.MenuBar;
            this.label12.Name = "label12";
            this.label12.MouseClick += new System.Windows.Forms.MouseEventHandler(this.menu_traduction_MouseClick);
            this.label12.MouseEnter += new System.EventHandler(this.menu_traduction_MouseEnter);
            this.label12.MouseLeave += new System.EventHandler(this.menu_traduction_MouseLeave);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.label14);
            this.panel5.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.panel5, "panel5");
            this.panel5.Name = "panel5";
            this.panel5.Click += new System.EventHandler(this.ExportarExcel_Click);
            this.panel5.MouseEnter += new System.EventHandler(this.panel5_MouseEnter);
            this.panel5.MouseLeave += new System.EventHandler(this.panel5_MouseLeave);
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.ForeColor = System.Drawing.Color.Coral;
            this.label13.Name = "label13";
            this.label13.Click += new System.EventHandler(this.ExportarExcel_Click);
            this.label13.MouseEnter += new System.EventHandler(this.panel5_MouseEnter);
            this.label13.MouseLeave += new System.EventHandler(this.panel5_MouseLeave);
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.ForeColor = System.Drawing.SystemColors.MenuBar;
            this.label14.Name = "label14";
            this.label14.Click += new System.EventHandler(this.ExportarExcel_Click);
            this.label14.MouseEnter += new System.EventHandler(this.panel5_MouseEnter);
            this.label14.MouseLeave += new System.EventHandler(this.panel5_MouseLeave);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label17);
            this.panel6.Controls.Add(this.label18);
            this.panel6.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.panel6, "panel6");
            this.panel6.Name = "panel6";
            this.panel6.Click += new System.EventHandler(this.panel6_Click);
            this.panel6.MouseEnter += new System.EventHandler(this.panel6_MouseEnter);
            this.panel6.MouseLeave += new System.EventHandler(this.panel6_MouseLeave);
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.ForeColor = System.Drawing.Color.Orchid;
            this.label17.Name = "label17";
            this.label17.Click += new System.EventHandler(this.panel6_Click);
            this.label17.MouseEnter += new System.EventHandler(this.panel6_MouseEnter);
            this.label17.MouseLeave += new System.EventHandler(this.panel6_MouseLeave);
            // 
            // label18
            // 
            resources.ApplyResources(this.label18, "label18");
            this.label18.ForeColor = System.Drawing.SystemColors.MenuBar;
            this.label18.Name = "label18";
            this.label18.Click += new System.EventHandler(this.panel6_Click);
            this.label18.MouseEnter += new System.EventHandler(this.panel6_MouseEnter);
            this.label18.MouseLeave += new System.EventHandler(this.panel6_MouseLeave);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(38)))), ((int)(((byte)(69)))));
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // MainSplit
            // 
            this.MainSplit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(38)))), ((int)(((byte)(69)))));
            resources.ApplyResources(this.MainSplit, "MainSplit");
            this.MainSplit.Name = "MainSplit";
            // 
            // MainSplit.Panel1
            // 
            this.MainSplit.Panel1.Controls.Add(this.panel_DBList);
            // 
            // MainSplit.Panel2
            // 
            this.MainSplit.Panel2.Controls.Add(this.SubSplit);
            // 
            // panel_DBList
            // 
            resources.ApplyResources(this.panel_DBList, "panel_DBList");
            this.panel_DBList.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel_DBList.Name = "panel_DBList";
            this.panel_DBList.Resize += new System.EventHandler(this.panel_DBList_Resize);
            // 
            // SubSplit
            // 
            this.SubSplit.BackColor = System.Drawing.SystemColors.ControlLight;
            resources.ApplyResources(this.SubSplit, "SubSplit");
            this.SubSplit.Name = "SubSplit";
            // 
            // SubSplit.Panel1
            // 
            this.SubSplit.Panel1.Controls.Add(this.dataGridView1);
            // 
            // SubSplit.Panel2
            // 
            this.SubSplit.Panel2.Controls.Add(this.txt_Editor);
            this.SubSplit.Panel2.Controls.Add(this.menu_SQLTools);
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Corbel", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.CadetBlue;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle1;
            resources.ApplyResources(this.dataGridView1, "dataGridView1");
            this.dataGridView1.GridColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // txt_Editor
            // 
            this.txt_Editor.BackColor = System.Drawing.Color.Salmon;
            this.txt_Editor.Controls.Add(this.panel7);
            this.txt_Editor.Controls.Add(this.panel1);
            resources.ApplyResources(this.txt_Editor, "txt_Editor");
            this.txt_Editor.Name = "txt_Editor";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.White;
            this.panel7.Controls.Add(this.richTextBox1);
            resources.ApplyResources(this.panel7, "panel7");
            this.panel7.Name = "panel7";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.richTextBox1, "richTextBox1");
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(38)))), ((int)(((byte)(69)))));
            this.panel1.Controls.Add(this.panel9);
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel2);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.label9);
            this.panel9.Controls.Add(this.label10);
            this.panel9.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.panel9, "panel9");
            this.panel9.Name = "panel9";
            this.panel9.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Help_Click);
            this.panel9.MouseEnter += new System.EventHandler(this.panel9_MouseEnter);
            this.panel9.MouseLeave += new System.EventHandler(this.panel9_MouseLeave);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.ForeColor = System.Drawing.SystemColors.MenuBar;
            this.label9.Name = "label9";
            this.label9.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Help_Click);
            this.label9.MouseEnter += new System.EventHandler(this.panel9_MouseEnter);
            this.label9.MouseLeave += new System.EventHandler(this.panel9_MouseLeave);
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.ForeColor = System.Drawing.SystemColors.MenuBar;
            this.label10.Name = "label10";
            this.label10.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Help_Click);
            this.label10.MouseEnter += new System.EventHandler(this.panel9_MouseEnter);
            this.label10.MouseLeave += new System.EventHandler(this.panel9_MouseLeave);
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.label7);
            this.panel8.Controls.Add(this.label8);
            this.panel8.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.panel8, "panel8");
            this.panel8.Name = "panel8";
            this.panel8.MouseEnter += new System.EventHandler(this.panel8_MouseEnter);
            this.panel8.MouseLeave += new System.EventHandler(this.panel8_MouseLeave);
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.ForeColor = System.Drawing.Color.Coral;
            this.label7.Name = "label7";
            this.label7.MouseEnter += new System.EventHandler(this.panel8_MouseEnter);
            this.label7.MouseLeave += new System.EventHandler(this.panel8_MouseLeave);
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.ForeColor = System.Drawing.SystemColors.MenuBar;
            this.label8.Name = "label8";
            this.label8.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Review_Click);
            this.label8.MouseEnter += new System.EventHandler(this.panel8_MouseEnter);
            this.label8.MouseLeave += new System.EventHandler(this.panel8_MouseLeave);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.panel4, "panel4");
            this.panel4.Name = "panel4";
            this.panel4.Click += new System.EventHandler(this.Load_Click);
            this.panel4.MouseEnter += new System.EventHandler(this.panel4_MouseEnter);
            this.panel4.MouseLeave += new System.EventHandler(this.panel4_MouseLeave);
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.ForeColor = System.Drawing.Color.YellowGreen;
            this.label5.Name = "label5";
            this.label5.Click += new System.EventHandler(this.Load_Click);
            this.label5.MouseEnter += new System.EventHandler(this.panel4_MouseEnter);
            this.label5.MouseLeave += new System.EventHandler(this.panel4_MouseLeave);
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ForeColor = System.Drawing.SystemColors.MenuBar;
            this.label6.Name = "label6";
            this.label6.Click += new System.EventHandler(this.Load_Click);
            this.label6.MouseEnter += new System.EventHandler(this.panel4_MouseEnter);
            this.label6.MouseLeave += new System.EventHandler(this.panel4_MouseLeave);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            this.panel2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseClick);
            this.panel2.MouseEnter += new System.EventHandler(this.panel2_MouseEnter);
            this.panel2.MouseLeave += new System.EventHandler(this.panel2_MouseLeave);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.Color.Turquoise;
            this.label4.Name = "label4";
            this.label4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseClick);
            this.label4.MouseEnter += new System.EventHandler(this.panel2_MouseEnter);
            this.label4.MouseLeave += new System.EventHandler(this.panel2_MouseLeave);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.SystemColors.MenuBar;
            this.label1.Name = "label1";
            this.label1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseClick);
            this.label1.MouseEnter += new System.EventHandler(this.panel2_MouseEnter);
            this.label1.MouseLeave += new System.EventHandler(this.panel2_MouseLeave);
            // 
            // menu_SQLTools
            // 
            this.menu_SQLTools.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(113)))), ((int)(((byte)(15)))));
            this.menu_SQLTools.Controls.Add(this.panel10);
            this.menu_SQLTools.Controls.Add(this.panel_Salida);
            resources.ApplyResources(this.menu_SQLTools, "menu_SQLTools");
            this.menu_SQLTools.Name = "menu_SQLTools";
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.label15);
            this.panel10.Controls.Add(this.label16);
            resources.ApplyResources(this.panel10, "panel10");
            this.panel10.Name = "panel10";
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.ForeColor = System.Drawing.SystemColors.MenuBar;
            this.label15.Name = "label15";
            // 
            // label16
            // 
            resources.ApplyResources(this.label16, "label16");
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.ForeColor = System.Drawing.SystemColors.MenuBar;
            this.label16.Name = "label16";
            // 
            // panel_Salida
            // 
            resources.ApplyResources(this.panel_Salida, "panel_Salida");
            this.panel_Salida.Name = "panel_Salida";
            // 
            // Form1
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HotTrack;
            this.Controls.Add(this.MainSplit);
            this.Controls.Add(this.menu_MainTools);
            this.Name = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.SizeChanged += new System.EventHandler(this.Form1_SizeChanged);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.menu_MainTools.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.menu_1.ResumeLayout(false);
            this.menu_1.PerformLayout();
            this.menu_traduction.ResumeLayout(false);
            this.menu_traduction.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.MainSplit.Panel1.ResumeLayout(false);
            this.MainSplit.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MainSplit)).EndInit();
            this.MainSplit.ResumeLayout(false);
            this.SubSplit.Panel1.ResumeLayout(false);
            this.SubSplit.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SubSplit)).EndInit();
            this.SubSplit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.txt_Editor.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.menu_SQLTools.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel menu_MainTools;
        private System.Windows.Forms.SplitContainer MainSplit;
        private System.Windows.Forms.SplitContainer SubSplit;
        private System.Windows.Forms.Panel txt_Editor;
        private System.Windows.Forms.FlowLayoutPanel menu_SQLTools;
        private System.Windows.Forms.FlowLayoutPanel panel_DBList;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel menu_1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Panel menu_traduction;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel_Salida;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
    }
}

