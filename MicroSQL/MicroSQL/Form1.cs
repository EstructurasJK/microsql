﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;

namespace MicroSQL
{

    public partial class Form1 : Form
    {
        // Variables usefull for UI.
        private View GUI = Program.GUI;
        List<Instructions> ReservatedWords = Syntax.ReservatedWords;
        private static Label lbl_Exit = new Label();
        private static string lastStatus = "";
        private static Create createForm = new Create();


        public Form1()
        {
            InitializeComponent();
            lbl_Exit.Font = new Font("Corbel", 12, FontStyle.Regular);
            panel_Salida.Controls.Add(lbl_Exit);
            lbl_Exit.Click += new EventHandler(lbl_Salidad_Click);
            lbl_Exit.ForeColor = Color.FromArgb(235, 235, 235);
            Message("Listo", "Listo");
            createForm.Show();
            createForm.Visible = false;
            SendObjects();
            LoadTables();
            Program.FillReservatedWords(Program.TraductionFilePath);
        }
    
        private void LoadTables()
        {
            DirectoryInfo directory = new DirectoryInfo(Program.TableFilePath);
            if (directory.GetFiles() != null)
            {
                FileInfo[] files = directory.GetFiles();
                for (int i = 0; i < files.Length; i++)
                {
                    StreamReader reader = new StreamReader(files[i].FullName);
                    string line = reader.ReadLine();
                    Program.TablesList.Add(line);
                    
                    string tableName = line;
                    string tableColumns = "";
                    while ((line = reader.ReadLine()) != null)
                    {
                        tableColumns += line + "|";
                    }
                    reader.Dispose();
                    reader.Close();
                    Program.AddDataBaseItem(tableName, tableColumns.Remove(tableColumns.Length - 1));
                    Program.SetCurrentTable(line);
                    
                }
            }
        }

        private void SendObjects()
        {
            Program.FormLayout = this.panel_DBList;
            Program.FormDataGrid = this.dataGridView1;
        }
        private void Form1_Resize(object sender, EventArgs e)
        {
            GUI.DataBaseItemResize(ref panel_DBList);
        }

        public static  void Message(string status, string error)
        {
            lbl_Exit.Width = status.Length * (int)lbl_Exit.Font.Size;
            lbl_Exit.Text = status;
            lastStatus = error;
        }

        private void lbl_Salidad_Click(object sender, EventArgs e)
        {
            if (lastStatus != "Listo")
            {
                MessageBox.Show(lastStatus.Replace("Clic aquí para más información", ""), lbl_Exit.Text, MessageBoxButtons.OK);
            }
        }

        private void panel_DBList_Resize(object sender, EventArgs e)
        {
            GUI.DataBaseItemResize(ref panel_DBList);
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            GUI.DataBaseItemResize(ref panel_DBList);
        }
        
        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            GUI.ReservatedWordsColor(ref richTextBox1);
        }

        private void menu_1_MouseClick(object sender, MouseEventArgs e)
        {
            createForm.Visible = true;
            createForm.Focus();
        }


        private void menu_traduction_MouseClick(object sender, MouseEventArgs e)
        {
            OpenFileDialog file = new OpenFileDialog();
            file.Title = "Seleccionar diccionario de instrucciones";
            if (file.ShowDialog() == DialogResult.OK)
            {
                if (file.FileName.Split('.')[1].ToUpper() == "INI" || file.FileName.Split('.')[1].ToUpper() == "CSV")
                {
                    Program.FillReservatedWords(file.FileName);
                }
                else
                {
                    Message("No se seleccionó un archivo válido", "El archivo de traducción no tiene un formato INI o CSV válido.");
                }
                return;
            }
        }
        

        private void Review_Click(object sender, MouseEventArgs e)
        {
            Program.ReviewInstructios(richTextBox1.Text);
        }

        private void Load_Click(object sender, EventArgs e)
        {
            if (Program.OperateInstructions(richTextBox1.Text))
            {
                richTextBox1.Text = "";
            }
        }

        private void Help_Click(object sender, MouseEventArgs e)
        {
            Process.Start(@"microsql_Manual.pdf");
        }
        private void DataBaseItems_DoubleClick(object sender, EventArgs e)
        {
            
        }

        private void TabControl_TabPage_DoubleClick(object sender, EventArgs e)
        {
            
        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {
        }

        private void menu_1_MouseEnter(object sender, EventArgs e)
        {
            menu_1.BackColor = Color.FromArgb(15, 38, 50);
        }

        private void menu_1_MouseLeave(object sender, EventArgs e)
        {
            menu_1.BackColor = Color.FromArgb(15, 38, 69);
        }

        private void menu_traduction_MouseEnter(object sender, EventArgs e)
        {
            menu_traduction.BackColor = Color.FromArgb(15, 38, 50);
        }

        private void menu_traduction_MouseLeave(object sender, EventArgs e)
        {
            menu_traduction.BackColor = Color.FromArgb(15, 38, 69);
        }
        
        private void panel5_MouseEnter(object sender, EventArgs e)
        {
            panel5.BackColor = Color.FromArgb(15, 38, 50);
        }

        private void panel5_MouseLeave(object sender, EventArgs e)
        {
            panel5.BackColor = Color.FromArgb(15, 38, 69);
        }

        private void panel2_MouseEnter(object sender, EventArgs e)
        {
            panel2.BackColor = Color.FromArgb(15, 38, 50);
        }

        private void panel2_MouseLeave(object sender, EventArgs e)
        {
            panel2.BackColor = Color.FromArgb(15, 38, 69);
        }

        private void panel4_MouseEnter(object sender, EventArgs e)
        {
            panel4.BackColor = Color.FromArgb(15, 38, 50);
        }

        private void panel4_MouseLeave(object sender, EventArgs e)
        {
            panel4.BackColor = Color.FromArgb(15, 38, 69);
        }

        private void panel8_MouseEnter(object sender, EventArgs e)
        {
            panel8.BackColor = Color.FromArgb(15, 38, 50);
        }

        private void panel8_MouseLeave(object sender, EventArgs e)
        {
            panel8.BackColor = Color.FromArgb(15, 38, 69);
        }

        private void panel9_MouseEnter(object sender, EventArgs e)
        {
            panel9.BackColor = Color.FromArgb(15, 38, 50);
        }

        private void panel9_MouseLeave(object sender, EventArgs e)
        {
            panel9.BackColor = Color.FromArgb(15, 38, 69);
        }



        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ExportarExcel_Click(object sender, EventArgs e)
        {
            Program.ExportExcel_Click();
            Message("Datos exportados a Excel", "Los datos de la tabla en pantalla se exportaron a una Hoja de Excel.");
        }

        private void panel6_MouseEnter(object sender, EventArgs e)
        {
            panel6.BackColor = Color.FromArgb(15, 38, 50);
        }

        private void panel6_MouseLeave(object sender, EventArgs e)
        {
            panel6.BackColor = Color.FromArgb(15, 38, 69);
        }

        private void panel6_Click(object sender, EventArgs e)
        {
            OpenFileDialog file = new OpenFileDialog();
            file.Title = "Seleccionar archivo CSV";
            file.DefaultExt = ".csv";
            string path = "";
            if (file.ShowDialog() == DialogResult.OK)
            {
                path = file.FileName;
                if (path.Split('.')[1].ToUpper() != "CSV")
                {
                    MessageBox.Show("El archivo seleccionado no es del tipo CSV.", "Formato incorrecto", MessageBoxButtons.OK);
                    Message("No se seleccionó un archivo CSV", "El archivo para exportar los datos a formato CSV no era formato CSV.");
                }
                else
                {
                    Program.ExportCSV_Click(path);
                    Message("Datos exportados al archivo CSV", "Los datos de la tabla en pantalla se exportaron al archivo CSV seleccionado.");
                }
            }
        }

        private void panel2_MouseClick(object sender, MouseEventArgs e)
        {
            richTextBox1.Text = "";
        }
    }
}
