﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace MicroSQL
{
    class View
    {

        public void AddDataBaseItem(ref FlowLayoutPanel panel_DBList, EventHandler DataBaseItems_Click, EventHandler DataBaseItems_DoubleClick, string DataBaseName, string DataBaseColumns)
        {
            Panel newPanel = new Panel();
            Label newName = new Label();
            Label newSymbol = new Label();
            Label newData = new Label();

            //newPanel.BackColor = Color.FromArgb(240, 96, 60);
            newPanel.BackColor = Color.FromArgb(245, 245, 245);
            if (panel_DBList.VerticalScroll.Visible == true)
            {
                newPanel.Width = panel_DBList.Width - 30;
            }
            else
            {
                newPanel.Width = panel_DBList.Width - 6;
            }

            newPanel.Height = 35;
            newPanel.Dock = DockStyle.Top;
            newPanel.Name = DataBaseName;


            newSymbol.Text = "2 ";
            newSymbol.ForeColor = Color.FromArgb(240, 96, 60);
            newSymbol.Font = new Font("Wingdings", 14, FontStyle.Bold);
            newSymbol.Location = new Point(10, 8);

            newName.Text = DataBaseName;
            newName.ForeColor = Color.DarkSlateGray;
            newName.Font = new Font("Corbel", 12, FontStyle.Bold);
            newName.Location = new Point(30, 10);
            newName.Width = newPanel.Width;
            newData.Name = DataBaseName + "_Data";
            List<string> columnsValues = DataBaseColumns.Split('|').ToList();
            string txt = "";
            for (int i = 0; i < columnsValues.Count; i++)
            {
                if (columnsValues[i].Length > 25)
                {
                    txt += "» " + columnsValues[i].Remove(25) + "..." + Environment.NewLine;
                    continue;
                }
                txt += "» " + columnsValues[i] + Environment.NewLine;
            }
            newData.Text = txt;
            newData.ForeColor = Color.DarkSlateGray;
            newData.Font = new Font("Corbel", 10, FontStyle.Bold);
            newData.Location = new Point(50, 35);
            newData.Height = 170;
            newData.Width = 25 * 12;
            newName.Click += new EventHandler(DataBaseItems_Click);
            newName.DoubleClick += new EventHandler(DataBaseItems_DoubleClick);
            newName.Cursor = Cursors.Hand;

            newName.Parent = newPanel;

            newPanel.Controls.Add(newName);
            newPanel.Controls.Add(newSymbol);
            newPanel.Controls.Add(newData);
            panel_DBList.Controls.Add(newPanel);
        }

        public void DataBaseItemResize(ref FlowLayoutPanel panel_DBList)
        {
            for (int i = 0; i < panel_DBList.Controls.Count; i++)
            {
                if (panel_DBList.VerticalScroll.Visible == true)
                {
                    panel_DBList.Controls[i].Width = panel_DBList.Width - 30;
                }
                else
                {
                    panel_DBList.Controls[i].Width = panel_DBList.Width - 6;
                }
            }
        }

        public void ReservatedWordsColor(ref RichTextBox richTextBox1)
        {
            int position = richTextBox1.SelectionStart;
            int initial = 0;
            int Seek = 0;
            richTextBox1.SelectAll();
            richTextBox1.SelectionColor = Color.Black;
            richTextBox1.SelectionFont = new Font(richTextBox1.Font, FontStyle.Regular);
            richTextBox1.SelectionStart += richTextBox1.TextLength;

            for (int i = 0; i < Syntax.ReservatedWords.Count; i++)
            {
                while(initial <= richTextBox1.TextLength - Syntax.ReservatedWords[i].Traduction.Length)
                {
                    Seek = richTextBox1.Find(Syntax.ReservatedWords[i].Traduction, initial, RichTextBoxFinds.WholeWord);
                    if (Seek != -1)
                    {
                        richTextBox1.SelectionStart = Seek;
                        richTextBox1.SelectionLength = Syntax.ReservatedWords[i].Traduction.Length;
                        richTextBox1.SelectionColor = Color.FromArgb(0, 187, 194);
                        richTextBox1.SelectionFont = new Font(richTextBox1.Font, FontStyle.Bold);
                        initial = Seek + Syntax.ReservatedWords[i].Traduction.Length;
                        continue;
                    }
                    initial++;
                }
                initial = 0;
            }

            for (int j = 0; j < Syntax.DataType.Count; j++)
            {
                while (initial <= richTextBox1.TextLength - Syntax.DataType[j].Length)
                 {
                    Seek = richTextBox1.Find(Syntax.DataType[j], initial, RichTextBoxFinds.WholeWord);
                    if (Seek != -1)
                    {
                        richTextBox1.SelectionStart = Seek;
                        richTextBox1.SelectionLength = Syntax.DataType[j].Length;
                        richTextBox1.SelectionColor = Color.FromArgb(240, 96, 60);
                        richTextBox1.SelectionFont = new Font(richTextBox1.Font, FontStyle.Bold);
                        initial = Seek + Syntax.DataType[j].Length;
                        continue;
                    }
                    initial++;
                }
                initial = 0;
            }

            richTextBox1.SelectionStart = position;
            richTextBox1.SelectionFont = new Font(richTextBox1.Font, FontStyle.Regular);
            richTextBox1.SelectionColor = Color.Black;
        }
        
    }
}
