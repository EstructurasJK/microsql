﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MicroSQL
{
    public partial class Create : Form
    {
        private int index = 0;

        public Create()
        {
            InitializeComponent();

        }

        private void Create_Load(object sender, EventArgs e)
        {

        }

        private void Create_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Visible = false;
            e.Cancel = true;
        }

  

        private void btn_Load_Click(object sender, EventArgs e)
        {
            string query = "";
            if (listBox1.Items.Count == 1)
            {
                MessageBox.Show("Es necesario que agregue una columna como mínimo.", "No se han añadido columnas.", MessageBoxButtons.OK);
            }
            else
            {
                query += Syntax.ReservatedWords.Find(x => x.Main.Equals("CREATE TABLE")).Traduction + " " + txt_name.Text + " ( ";
                for (int i = 0; i < listBox1.Items.Count; i++)
                {
                    query += listBox1.Items[i] + ", ";
                }
                query = query.Remove(query.Length - 2) + " )";
                Clipboard.SetDataObject(query);
                MessageBox.Show("La instrucción se ha copiado a su portapapeles. Pegue la instrucción (Ctrl + V) en el editor SQL y ejecutela." , "Instrucción generada correctamente.", MessageBoxButtons.OK);
                Clear();
                txt_name.Text = "";
                btn_Load.Enabled = false;
                txt_name.Focus();
                query = listBox1.Items[0].ToString();
                listBox1.Items.Clear();
                listBox1.Items.Add(query);
                this.Close();
            }
        }

        private void GroupBox_Enter(object sender, EventArgs e)
        {

        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            string row = "";
            if (items.Text != "" && txt_int1.Text != "")
            {
                switch (items.Text)
                {
                    case "INT":
                        row = txt_int1.Text + " " + items.Text;
                        break;
                    case "VARCHAR":
                        row = txt_int1.Text + " " + items.Text +"(" + num_int1.Value + ")";
                        break;
                    case "DATETIME":
                        row = txt_int1.Text + " " + items.Text;
                        break;
                    default:
                        break;
                }
                listBox1.Items.Add(row);
                Clear();
            }
        }

        private void Clear()
        {
            items.SelectedIndex = -1;
            txt_int1.Text = "";
            txt_int1.Enabled = false;
            btn_add.Enabled = false;
            num_int1.Value = 1;
            listBox1.Focus();

        }
        private void items_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (items.Text != "" && txt_int1.Text != "")
            {
                btn_add.Enabled = true;
            }
            else
            {
                txt_int1.Enabled = true;
                btn_add.Enabled = false;
            }
            if (items.Text == "VARCHAR")
            {
                num_int1.Enabled = true;
            }
            else
            {
                num_int1.Enabled = false;
            }
        }

        private void txt_int1_TextChanged(object sender, EventArgs e)
        {
            if (items.Text != "" && txt_int1.Text != "")
            {
                btn_add.Enabled = true;
            }
            else
            {
                btn_add.Enabled = false;
            }
            txt_int1.Text = txt_int1.Text.Replace(' ', '_');
            txt_int1.SelectionStart = txt_int1.TextLength;
        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != 0)
            {
                listBox1.Items.RemoveAt(listBox1.SelectedIndex);
            }
        }

        private void txt_name_TextChanged(object sender, EventArgs e)
        {
            if (txt_name.Text != "")
            {
                btn_Load.Enabled = true;
            }
            txt_name.Text = txt_name.Text.Replace(' ', '_');
            txt_name.SelectionStart = txt_name.TextLength;
        }
    }
}
