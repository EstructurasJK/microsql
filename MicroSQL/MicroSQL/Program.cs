﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Microsoft.Office.Interop.Excel;

namespace MicroSQL
{
    static class Program
    {
        // Variables
        public static View GUI = new View();
        public static Table CurrentTable = null;
        public static List<string> TablesList = new List<string>();
        public static string TreeFilePath = "..\\..\\microSQL\\arbolesb\\";
        public static string TableFilePath = "..\\..\\microSQL\\tablas\\";
        public static string TraductionFilePath = "..\\..\\microSQL\\microSQL.ini";
        public static Syntax CodeSyntax = new Syntax();
        public static SQLCode OperationsSQL = new SQLCode();


        /* OBJECTS FROM FORM*/

        public static FlowLayoutPanel FormLayout;
        public static DataGridView FormDataGrid;

        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {

            System.Windows.Forms.Application.EnableVisualStyles();
            System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);

            Syntax.DataType.Add("INT PRIMARY KEY");
            Syntax.DataType.Add("INT");
            Syntax.DataType.Add("VARCHAR");
            Syntax.DataType.Add("DATETIME");

            System.Windows.Forms.Application.Run(new Form1());
            FormLayout = new FlowLayoutPanel();
            FormDataGrid = new DataGridView();
        }

        private static void DataBaseItems_Click(object sender, System.EventArgs e)
        {
            System.Windows.Forms.Label item = sender as System.Windows.Forms.Label;
            if (item != null)
            {
                Panel parent = item.Parent as Panel;
                if (parent.Height == 35)
                {
                    parent.Height = 70 * parent.Controls.Count;
                }
                else
                {
                    parent.Height = 35;
                }
            }
        }

        public static void DropTable(string tableName)
        {
            if (TablesList.Count > 0)
            {
                SetCurrentTable(TablesList[0]);
            }
            else
            {
                CurrentTable = null;
            }
            File.Delete(TableFilePath + tableName + ".tabla");
            File.Delete(TreeFilePath + tableName + ".txt");
            foreach (Panel item in FormLayout.Controls)
            {
                if (item != null && item.Name == tableName)
                {
                    FormLayout.Controls.Remove(item);
                    break;
                }

            }
            FormDataGrid.Columns.Clear();
        }

        public static void DataBaseItems_DoubleClick(object sender, System.EventArgs e)
        {
            string columns = "";
            System.Windows.Forms.Label name = sender as System.Windows.Forms.Label;
            SetCurrentTable(name.Text);
            for (int i  = 0; i  < CurrentTable.GetColumns().Count; i ++)
            {
                columns += CurrentTable.GetColumns()[i].ColumnName + "|";
            }
            List<Row> list = CurrentTable.SelectFrom(columns.Remove(columns.Length - 1));
            FillDataGridView(list);
        }

        public static void FillDataGridView(List<Row> Results)
        {
            FormDataGrid.Columns.Clear();
            for (int i = 0; i < CurrentTable.GetColumns().Count; i++)
            {
                FormDataGrid.Columns.Add(CurrentTable.GetColumns()[i].ColumnName, CurrentTable.GetColumns()[i].ColumnName);
            }
            //imagino que es algo así, pero si no es lo que nos sirve hay que cambiarlo es que no sé que me recornan tus rows cuando solo quiero una columna ¿nada "" en las demás?
            for (int i = 0; i < Results.Count; i++)
            {
                FormDataGrid.Rows.Add();
                for (int m = 0; m < Results[i].Registers.Count; m++)
                {
                    if (Results[i].Registers[m] != "")
                    {
                        FormDataGrid.Rows[i].Cells[m].Value = Results[i].Registers[m];
                    }
                }
            }
        }

        public static void ExportCSV_Click(string path)
        {
            StreamWriter writer = new StreamWriter(path);
            string line = "";
            for (int i = 0; i < FormDataGrid.Rows.Count; i++)
            {
                for (int j = 0; j < FormDataGrid.Columns.Count; j++)
                {
                    if (FormDataGrid[j, i].Value != null)
                    {
                        line += FormDataGrid[j, i].Value.ToString() + ", ";
                    }
                    else
                    {
                        line += "~, ";
                    }
                }
                line = line.Remove(line.Length - 2) + Environment.NewLine;
            }
            writer.Write(line);
            writer.Flush();
            writer.Dispose();
            writer.Close();
        }

        public static void ExportExcel_Click()
        {
            Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();
            excel.Application.Workbooks.Add(true);
            int cellIndex = 0;
            int rowIndex = 0;

            foreach (DataGridViewColumn column in FormDataGrid.Columns)
            {
                cellIndex++;
                excel.Cells[1, cellIndex] = column.Name;
            }

            foreach (DataGridViewRow row in FormDataGrid.Rows)
            {
                rowIndex++;
                cellIndex = 0;
                foreach (DataGridViewColumn column in FormDataGrid.Columns)
                {
                    cellIndex++;
                    excel.Cells[rowIndex + 1, cellIndex] = row.Cells[column.Name].Value;
                }
            }

            excel.Visible = true;
        }

        public static bool SetCurrentTable(string tableName)
        {
            if (CurrentTable == null || CurrentTable.TableName != tableName)
            {
                if (TablesList.Find(x => x == tableName) != null)
                {
                    CurrentTable = new Table(tableName);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }
        public static void AddDataBaseItem(string tableName, string columnValues)
        {
            GUI.AddDataBaseItem(ref FormLayout, DataBaseItems_Click, DataBaseItems_DoubleClick, tableName, columnValues);
        }

        public static void CreateNewTable(string tableName, string columnValues)
        {
            CurrentTable = new Table(tableName, columnValues);
            AddDataBaseItem(tableName, columnValues);
        }


        public static void FillReservatedWords(string path = null)
        {
            Syntax.ReservatedWords.Clear();
            string line = "";



            if (path != null && File.Exists(path))
            {
                StreamReader NewInstructions = new StreamReader(path);
                List<string> MainAndTraduction;
                while ((line = NewInstructions.ReadLine()) != null)
                {
                    if (line != "")
                    {
                        MainAndTraduction = line.Split(',').ToList();
                        Syntax.ReservatedWords.Add(new Instructions(MainAndTraduction[0].Trim().ToUpper(), MainAndTraduction[1].Trim().ToUpper()));
                    }
                }

                if (Syntax.ReservatedWords.Count != 11)
                {
                    NewInstructions.Close();
                    Form1.Message("Archivo inválido", "El archivo tiene más o menos instrucciones de las definidas en este gestor.\nSe seguirá trabajando con las instrucciones de serie.");
                    FillStandarWords();
                }
                else
                {
                    Form1.Message("Idioma cambiado correctamente", "El idioma del editor se ha cambiado correctamente.");
                }
                NewInstructions.Dispose();
                NewInstructions.Close();
            }
            else
            {
                FillStandarWords();
            }
            StreamWriter writer = new StreamWriter(TraductionFilePath);
            line = "";
            for (int i = 0; i < Syntax.ReservatedWords.Count; i++)
            {
                line += Syntax.ReservatedWords[i].Main + "," + Syntax.ReservatedWords[i].Traduction + Environment.NewLine;
            }
            writer.WriteLine(line);
            writer.Dispose();
            writer.Close();
            
        }

        private static void FillStandarWords()
        {
            Syntax.ReservatedWords.Add(new Instructions("SELECT", "SELECT"));
            Syntax.ReservatedWords.Add(new Instructions("FROM", "FROM"));
            Syntax.ReservatedWords.Add(new Instructions("DELETE", "DELETE"));
            Syntax.ReservatedWords.Add(new Instructions("WHERE", "WHERE"));
            Syntax.ReservatedWords.Add(new Instructions("CREATE TABLE", "CREATE TABLE"));
            Syntax.ReservatedWords.Add(new Instructions("DROP TABLE", "DROP TABLE"));
            Syntax.ReservatedWords.Add(new Instructions("INSERT INTO", "INSERT INTO"));
            Syntax.ReservatedWords.Add(new Instructions("VALUES", "VALUES"));
            Syntax.ReservatedWords.Add(new Instructions("GO", "GO"));
            Syntax.ReservatedWords.Add(new Instructions("UPDATE", "UPDATE"));
            Syntax.ReservatedWords.Add(new Instructions("SET", "SET"));
        }

        public static void ReviewInstructios(string richTextBoxText)
        {
            CodeSyntax.TextSintaxis(richTextBoxText);
        }
        public static bool OperateInstructions(string richTextBoxText)
        {
            List<string> SQLCode = CodeSyntax.TextSintaxis(richTextBoxText);
            if (SQLCode.Count == 0)
                return false;
            return OperationsSQL.Operate(0, SQLCode);
            
        }
    }
}
